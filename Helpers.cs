﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public static class Helpers
    {
        public static bool IsBlank(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return true;
            }
            return false;
        }

        //created a neasted while loop to minimize the number of times the user have to do the right thing
        //once the user did not adhere to instructions for 3 concecutive times the application terminates

        public static int count = 0;
        public static bool TerminateApp(string input, bool appCondition)
        {
            while (count < 3 && appCondition)
            {
                while (Helpers.IsBlank(input) && count < 2)
                {
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine($"Please fill in your name in the form.Donot leave blank.");
                    Console.ResetColor();
                    input = Console.ReadLine();
                    count++;
                }
                while (Helpers.IsBlank(input) && count == 2)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"Please you will be logged out,if name is still blank");
                    Console.ResetColor();
                    Console.WriteLine($"Enter your name: ");
                    input = Console.ReadLine();
                    if (Helpers.IsBlank(input))
                    {
                        appCondition = false;
                    }
                    count++;
                }
                
            }
            //Exits the loop and terminates the program
            count = 0;
            return appCondition;
        }

        
    }
}
        

    

