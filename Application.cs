﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public static class Application
    {
        
        public static void Run()
        {
           var stringbuilder = new StringBuilder();
           Console.Write("Hello, welcome to my App");
           Console.Write("Enter your FirstName: ");
           var firstName = Console.ReadLine();

            var Islogged = true;
            while (Helpers.TerminateApp(firstName, Islogged) == false)
            {
                End();
                return;
            }
           
            Console.Write("Enter your Lastname: ");
            var lastName = Console.ReadLine();
            Console.WriteLine();

            while (Helpers.TerminateApp(lastName, Islogged) == false)
            {
                End();
                return;
            }

            Console.Write("Enter email: ");
            var email = Console.ReadLine();
            bool IsValidEmail()
            {
                try
                {
                    var mail = new System.Net.Mail.MailAddress(email);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            while (Helpers.IsBlank(email))
            {
                Console.WriteLine("Please put in a valid email. Email address is required?.");
                Console.WriteLine("Enter email");
                email = Console.ReadLine();
            }
            while (IsValidEmail().Equals(false))
            {
                Console.WriteLine("please enter a valid email address");
                email = Console.ReadLine();
            }
            
            Console.WriteLine("Enter birthday: ");
            var birthday = Console.ReadLine();
            Console.WriteLine();
            while (Helpers.IsBlank(birthday))
            {
                Console.WriteLine("Please enter your date of birth in the following order mm/dd/yyyy.");
                birthday = Console.ReadLine();
            }
           
            Console.WriteLine("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say");
            var gender = Console.ReadLine();
            Console.WriteLine();
            var selectedGender = GenderSelection(gender);
            if (gender != "1" && gender != "2" && gender != "3")
            {
                Console.WriteLine("Invalid Gender Selection");
            }
            while (Helpers.IsBlank(gender))
            {
                Console.WriteLine("Your have to choose from 1 to 3 that which first describe you.");
                gender = Console.ReadLine();
            }
           
            Console.WriteLine("Enter Password");
            var password = Console.ReadLine();

            Console.WriteLine("Confirm Password");
            var confirmPassword = Console.ReadLine();

            var formData = new Register
            { 
                FirstName = firstName, 
                LastName = lastName,
                Email = email,
                Birthday = DateTime.Parse(birthday),
                Password = password,
                ConfirmPassword = confirmPassword,
                Gender = selectedGender
            };

            AccountService.Register(formData);
        }

        public static void End()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Process End... GoodBye!!!");
            Console.ResetColor();
            return;
        }

        public static Gender GenderSelection(string gender)
        {
            
            switch (gender)
            {
                case "1":
                    return Gender.Male;       
                case "2":
                    return Gender.Female;
                case "3":
                    return Gender.PreferNotToSay;
                default:
                    return Gender.SelectGender;
            }
        }
    }
}
